// console.log(`Hello world`);

// [SECTION] Objects

/*
    - An object is a data type that is used to represent real world objects
    - It is a collection of related data and/or functionalities
    - In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
    - Information stored in objects are represented in a "key:value" pair
    - A "key" is also mostly referred to as a "property" of an object
	- Different data types may be stored in an object's property creating complex data structures
*/

// Creating objects using objects initializers/literal notation

/*
    - This creates/declares an object and also initializes/assigns it's properties upon creation
    - A cellphone is an example of a real world object
    - It has it's own properties such as name, color, weight, unit model and a lot of other things
    - Syntax
        let objectName = {
            keyA: valueA,
            keyB: valueB
        }
*/

let cellphone = {
    name: 'Nokia 3210',
    manufactureDate: 1999
};

console.log('Result from creating objects using initializers/literal notation:');
console.log(cellphone);
console.log(typeof cellphone);


// Creating objects using a constructor function
/*
    - Creates a reusable function to create several objects that have the same data structure
    - This is useful for creating multiple instances/copies of an object
    - An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
    - Syntax
        function ObjectName(keyA, keyB) {
            this.keyA = keyA;
            this.keyB = keyB;
        }
*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters

// this keyword - to store value
function Laptop(name, manufactureDate){
    this.name = name;
    this.manufactureDate = manufactureDate;
}

let laptop = new Laptop(`MacBook Pro`, 2021); // `new` keyword
console.log(`Result from creating objects using objects constructors: `)
console.log(laptop);
							// name 	// manufacturedDate
let myLaptop = new Laptop(`MacBook Air`, 2020);
console.log(`Result from creating objects using objects constructors: `)
console.log(myLaptop);

let oldLaptop = Laptop(`Portal R2E CCMC`, 1980);
console.log(`Result from creating objects without the new keyword: `)
console.log(oldLaptop);

let myNewLaptop = new Laptop(`Nitro 5`, 2000);
console.log(`Result from creating objects new keyword: `)
console.log(myNewLaptop);

// make sure to use new keyword to store data / values succesfully;

// Creating empty objects
let computer = { };
// let emptyArray = [];
let myComputer = new Object();


// [SECTION] Accessing Object Properties

console.log(`---------------------------------`)
console.log(`=> Dot notation: `);

// Using dot notation					// object name 	// property/key to access
console.log(`Result from dot notation: ${myLaptop.name}`);
console.log(`Result from dot notation: ${myLaptop.manufactureDate}`);
console.log(`Result from dot notation: ${cellphone.name}`);


console.log(`---------------------------------`)
console.log(`=> Square bracket notation: `);

// Using the square bracket notation
console.log(`Result from square bracket notation: ${myLaptop['name']}`);
//console.log(`Result from square bracket notation: ${myNewLaptop['manufactureDate']}`);
console.log(`Result from square bracket notation: ${cellphone['name']}`);

console.log(`---------------------------------`)
console.log(`=> Accessing object value inside an array: `);

// objects inside an array
let array = [laptop, myLaptop];

// console.log(laptop['name']);
console.log(array[0][`name`]);

// dot notation
console.log(array[0].name);


// [Section] Initializing/Adding/Deleting/Reassigning Object Properties
/*
    - Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
    - This is useful for times when an object's properties are undetermined at the time of creating them
*/

console.log(`---------------------------------`)
console.log(`=> Initializing an object property `);

// declared empty
let car = {};

// initialization using dot notation
car.name = `Honda Civic`;
console.log(`Result of adding properties using dot notation: `);
console.log(car);

// initialization using square bracket notation to access property name with spaces
car[`manufacture date`] = 2019;
console.log(car[`manufacture date`]);
console.log(car[`Manufacture Date`]); // undefined because javascript is case-sensitive
console.log(car.ManufactureDate); // undefined, it is much effective if we use square bracket to access property name with spaces

console.log(`Result of adding properties using square bracket notation: `);
console.log(car);

// Deleting object properties
delete car[`manufacture date`];
console.log(`Result from deleting properties: `);
console.log(car);

// Re-assigning object properties

// Re-assigning through dot notation
/*car.name = `Dodge Charger R/T`;
console.log(`Result from reassigning properties: `);
console.log(car);
*/

// Re-assigning through square bracket notation
car[`name`] = `Dodge Charger R/T`;
console.log(`Result from reassigning properties: `);
console.log(car);

// we can reassign values using dot notation and square bracket notation

// -------------------------------------------------------------------------

// [Section] Object Methods

/*
    - A method is a function which is a property of an object
    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
    - Methods are useful for creating object specific functions which are used to perform tasks on them
    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
*/

// function inside an object property
let ferson = {
    name: `Minty`,

    // method
    talk: function(){
        console.log(`Hello my name is ${this.name}`);
        // we use `this` keyword to access a property that co-exist inside the object
    }
    /*walk = function(){
        console.log(`${this.name} walked 25 steps forward.`);
    }*/
}

console.log(ferson);
console.log(`Result of object methods: `);
ferson.talk();

// we could also create a method outside the object code

    // Adding methods to objects
    // Syntax for initializing/adding a property or re-assigning a property value
    // `ferson` - object name
    // `walk` - new property

// Initialize or create new property
ferson.walk = function(){
    console.log(`${this.name} walked 25 steps forward.`);
};
ferson.walk();

//Re-assigning a property value
ferson.walk = function(){
    console.log(`${this.name} walked 30 steps forward.`);
};
ferson.walk();
console.log(ferson);
// console.log(ferson.walk);

// Methods are useful for creating reusable functions that perform tasks related to objects

/*
    Scenario:
    1. We would like to create a game that would have several pokemon interact with each other
    2. Every pokemon would have the same set of stats, properties and functions
*/

function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;

    this.tackle = function(target) {
        console.log(`${this.name} tackled ${target.name}`);

        target.health -= this.attack;
        console.log(`${target.name} health is now reduced to ${target.health}.`);

        if(target.health <= 0) {
            target.faint();
        }
    }
    this.faint = function() { 
        console.log(`${this.name} fainted`);
    }
}

let pickachu = new Pokemon(`Pikachu`, 99);
console.log(pickachu);

let rattata = new Pokemon(`Rattata`, 5);
console.log(rattata);

pickachu.tackle(rattata);

rattata.tackle(pickachu);









